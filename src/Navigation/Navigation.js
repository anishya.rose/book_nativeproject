import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';

const createStackNavigatorOptions={
    headerShown:false
}
const AppNavigator =createStackNavigator({
    Login:{screen:Login},
},
{
    defaultNavigationOptions: StackNavigatorOptions
}
);
export default createAppContainer(AppNavigator);