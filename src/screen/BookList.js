import React from 'react';
import { SafeAreaView, Image,View, FlatList, StyleSheet, Text, StatusBar, TouchableHighlight } from 'react-native';

import { useNavigation } from '@react-navigation/native';


const Item = ({ id, publisher,title,year,isbn,cover,author,deleteUser }) => {
  const navigation = useNavigation();
  return(
  <View style={styles.item}>
    <View style={{flex: 1, flexDirection:'row'}}>
   
      <View style={{flex: 5}}>
            <TouchableHighlight onPress={()=>{
              navigation.navigate('OpenBook',{userId:id});  
            }}>
            <Image
              style={styles.cover}
              source={{
                uri: cover,
              }}
            />
            </TouchableHighlight>
          </View>
    
      <View style={{flex: 1}}>
            
        <TouchableHighlight onPress={()=>{
          console.log("do edit x :"+ id);
          navigation.navigate('AddBook',{userId:id});  
        }}>
        <Image
          style={styles.tinyLogo}
          source={{
            uri: 'http://training.pyther.com/icons/edit.png?9',
          }}
        />
        </TouchableHighlight>
      </View>

      <View style={{flex: 1}}>
        <TouchableHighlight onPress={()=>{
          deleteUser(id);
        }}>
        <Image
          style={styles.tinyLogo}
          source={{
            uri: 'http://training.pyther.com/icons/delete.png?9',
          }}
        />
        </TouchableHighlight>
      </View>
    </View>
    
    <View>
         <Text style={styles.publisher}>{publisher}</Text>
         <Text style={styles.title}>{title}</Text>
         <Text style={styles.year}>{year}</Text>
         <Text style={styles.isbn}>{isbn}</Text>
         <Text style={styles.author}>{author}</Text>
    </View>

</View>
)};
        
const ListApp = ({users, deleteUser, editUser,}) => {
  console.log(users);
  console.log(".............................")
  const navigation = useNavigation();
  const renderItem = ({ item }) => (
    <Item id={item.id} publisher={item.publisher} title={item.title}  isbn={item.isbn} year={item.year} cover={item.cover} author={item.author} deleteUser={deleteUser} editUser={editUser} navigation={navigation}/>
  
    );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={users}
        renderItem={renderItem}
        
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
  }
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  publisher: {
    fontSize: 16,
    color:'white',
  },
  title: {
    fontSize:24,
    color:'white',
  },
  year:{
    fontSize:16,
    color:'white',
  },
  author:{
    fontSize:20,
    color:'white',
    fontFamily:'Arial',
  },

  isbn:{
    fontSize:16,
    color:'white',
  },
  others: {
    fontSize: 10,
  },
  tinyLogo: {
    width: 30,
    height: 30,
  },

  cover :{
  
      width :200,
      height :200,
  }
});

export default ListApp;