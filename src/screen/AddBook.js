import React,{useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  TextInput,
} from 'react-native';
import UserList from './BookList';
import AsyncStorage from '@react-native-community/async-storage';
import {getBook,getBookById,addBook,updateBook,deleteBook,reloadBook} from '../service/BookServiceAPI';
const defaultUsers =[];

  const AddBook = ({route, navigation}) => {
   // const {userId} = route.params;
    const [userId, setUserId] = React.useState('0');
    const [publisher, setPublisher] = React.useState("");
    const [title, setTitle] = React.useState("");
    const [isbn, setIsbn] = React.useState("");
    const [year, setYear] = React.useState("");
    const [cover,setCover] =React.useState("");
    const [author,setAuthor] =React.useState("");
    const [users, setUsers] = React.useState(defaultUsers);
    const [label, setLabel] = React.useState("Add");

  var reloadBook = async() =>{
    var book = await getBook();
    setUsers(book); 
  }

  var loadBook = async (currentUserId) =>{
    let currentUser = await getBookById(currentUserId);
      setPublisher(currentUser.publisher);
      setUserId(currentUser.id);
      setTitle(currentUser.title);
      setIsbn(currentUser.isbn);
      setYear(currentUser.year);
      setAuthor(currentUser.author);
      setLabel("Update");
  }


  useEffect(()=>{
    if(route.params && route.params.userId){
        loadBook(route.params.userId);
    }
  },[]);

  var resetForm = () =>{
    setPublisher('');
    setUserId('0');
    setYear('');
    setIsbn('');
    setTitle('');
    setCover('');
    setAuthor('');
  
    setLabel('Add');
   // reloadBook();
  }

  var editUser = (id) =>{
    console.log("edit userAdd:"+id);
    let tempUsers = users.filter((user)=>(user.id == id));
    if(tempUsers.length > 0){
      let currentUser = tempUsers[0];
      setPublisher(currentUser.publisher);
      setUserId(currentUser.id);
      setYear(currentUser.year);
      setIsbn(currentUser.isbn);
      setTitle(currentUser.title);
      setCover(currentUser.cover);
      setAuthor(currentUser.author);
    }
  }
  var  addUpdateBook =async () =>{
    if(userId == "0"){ //add
      console.log(">> add");
      let user ={id:Date.now() +'e',publisher,author,year,isbn,title,cover};
        await addBook(user);
      navigation.navigate('Dashboard');
      // setUsers([...users, user]);
    }else{ //update
      console.log(" update");
/*let tempUsers = [...users];
      tempUsers.map((item)=>{
        if(item.id == userId){
          console.log("record updated.");
          item.publisher = publisher;
          item.year = year;
          item.isbn =isbn;
          item.title =title;
          item.cover=cover;
          item.author=author;
        }
      })*/
      let user ={id:userId,publisher,title,year,isbn,author,cover};
       await updateBook(user);
      reloadBook();
    }
    navigation.navigate('Dashboard');
    //  console.log("tempUsers"+JSON.stringify(tempUsers));
    //  setUsers(tempUsers);
    
    // resetForm();
  }
 var deleteUser = (id) =>{
    console.log("delete book:"+id);
    let tempUsers = users.filter((user)=>(user.id != id));
    setUsers(tempUsers);
  }
  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
            <Text style={styles.sectionTitle}>Add Books</Text>
            <View>
            <TextInput
            style={{ height: 40, borderColor: 'white', borderWidth: 2, marginBottom:20 }}
            placeholder="Enter Publisher"
            onChangeText={text => setPublisher(text)}
            value={publisher}
            />
            <TextInput placeholder="year"
            style={{ height: 40, borderColor: 'white', borderWidth: 2 , marginBottom:20}}
            onChangeText={text => setYear(text)}
            placeholder="Enter Year"
            value={year}
            />
            <TextInput
            style={{ height: 40, borderColor: 'white', borderWidth: 2 , marginBottom:20}}
            onChangeText={text => setIsbn(text)}
            placeholder="Enter Isbn"
            value={isbn}
            />
            <TextInput
            style={{ height: 40, borderColor: 'white', borderWidth: 2, marginBottom:20}}
            onChangeText={text => setTitle(text)}
            placeholder="Enter Title"
            value={title}
            />

            <TextInput
            style={{ height: 40, borderColor: 'white', borderWidth: 2, marginBottom:20}}
            onChangeText={text => setAuthor(text)}
            placeholder="Enter Author"
            value={author}
            />
            
            <Button color="darkpink" title={label} onPress={addUpdateBook}></Button>
            </View>
            
            <View/>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'white',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor:  "#F089A5",
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: 'black',
    marginBottom:20
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },

});

export default AddBook;
