import React,{useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  TextInput,
} from 'react-native';
import UserList from './BookList';
import Menu from '../Menu';

import {getBook,addBook,updateBook,deleteBook,editBook} from '../service/BookServiceAPI';
import AsyncStorage from '@react-native-community/async-storage';
const defaultUsers =[];

const Book = ({navigation}) => {
  const [userId, setUserId] = React.useState('0');
  const [publisher, setPublisher] = React.useState("");
  const [author, setAuthor] = React.useState("");
  const [title, setTitle] = React.useState("");
  const [isbn, setIsbn] = React.useState("");
  const [year, setYear] = React.useState("");
  const [users, setUsers] = React.useState(defaultUsers);
  const [cover,setCover] =React.useState("");
  const [label, setLabel] = React.useState("Add");
  const [loginName,setLoginName] = React.useState("");

   var reloadBook = async() =>{
    var book = await getBook();
     setUsers(book); 
   }
   useEffect(()=>{
    AsyncStorage.getItem('email').then((value)=> setLoginName(value));
    const unsubscribe = navigation.addListener('focus', (data) => {
         if(data.target.startsWith("Dashboard")){
            reloadBook();
         }
       });
       return unsubscribe;
   },[navigation]);

  var resetForm = () =>{
    setPublisher('');
    setUserId('0');
    setAuthor('');
    setTitle('');
    setIsbn('');
    setYear('');
    setCover('');
    setLabel('Add');
  }

 var editUser = (id) =>{
    console.log("edit user 01:"+id);
   navigation.navigate('AddBook');        
  }
 var addUpdateUser = async() =>{
  }
 var deleteUser = (id) =>{
    console.log("delete user:"+id);
    let tempUsers = users.filter((user)=>(user.id != id));
    setUsers(tempUsers);
  }
  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
          <Menu navigation={navigation}/>
            <View style={styles.sectionContainer}>
            <Text style={styles.sectionTitle}>Books</Text>
          
            <UserList users={users} 
            deleteUser={deleteUser} 
            editUser={editUser}></UserList>
           
           
            <View/>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'white',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: "#F089A5",
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
    backgroundColor: "#F089A5"
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: 'white',
    marginBottom:20,
    marginleft:60
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
   
  },
});

export default Book;
