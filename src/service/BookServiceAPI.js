var apiEndPoint = "http://training.pyther.com:3000/api/book";
export var getBook = () =>{
    return fetch(apiEndPoint, {
        method: 'get',
            headers: {
            'Content-Type': 'application/json;charset=utf-8'
              }
        })
      .then(response => response.json())
      .then(response => {
        console.log(JSON.stringify(response));
        return(response);
        }).catch(function(error) {
            console.log(error);
        });
}

export var addBook = (book) =>{
    return fetch(apiEndPoint, {
        method: 'post',
            headers: {
            'Content-Type': 'application/json;charset=utf-8'
              },
              body:JSON.stringify(book)
        })
      .then(response => response.json())
      .then(response => {
        return(response);
        }).catch(function(error) {
            console.log(error);
        });
}

export var deleteBook = (book) =>{
    return fetch(apiEndPoint, {
        method: 'delete',
            headers: {
            'Content-Type': 'application/json;charset=utf-8'
              },
              body:JSON.stringify(book)
        })
      .then(response => response.json())
      .then(response => {
        return(response);
        }).catch(function(error) {
            console.log(error);
        });
}
export var getBookById =  (id) =>{
    return fetch(apiEndPoint+"/"+id, {
        method: 'get',
            headers: {
            'Content-Type': 'application/json;charset=utf-8'
              }
        })
      .then(response => response.json())
      .then(response => {
        return(response);
        }).catch(function(error) {
            console.log(error);
        });
}
export var updateBook = (book) =>{
    return fetch(apiEndPoint, {
        method: 'put',
            headers: {
            'Content-Type': 'application/json;charset=utf-8'
              },
              body:JSON.stringify(book)
        })
      .then(response => response.json())
      .then(response => {
        return(response);
        }).catch(function(error) {
            console.log(error);
        });
}