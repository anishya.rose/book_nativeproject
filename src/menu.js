import React from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
export default class Menu extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {

    return (
        <View style={styles.sectionContainer}>
      <View style={{flex: 1, flexDirection: 'row'}}>
      <Button color="darkpink"
        onPress={() => this.props.navigation.navigate('AddBook')}
        title="Add"
      />
        <Button color="darkpink"
        onPress={() => this.props.navigation.navigate('Login')}
        title="Logout"
      />
           
                 
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F089A5",
    alignItems: 'center',
    justifyContent: 'center',
  },
});