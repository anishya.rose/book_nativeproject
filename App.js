import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Login from './src/screen/Login';
import Dashboard from './src/screen/Dashboard';
import AddBook from './src/screen/AddBook';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator,createMaterialTopTabNavigator } from '@react-navigation/stack';
//import { StyleSheet, Text, View } from 'react-native';
//import ImagesExample from './ImagesExample.js'
const Stack = createStackNavigator();
//import BookList from  './src/screen/BookList';


 function App() {
  return (
    <NavigationContainer>
     <Stack.Navigator>

     <Stack.Screen name="Login" component={Login} />
     <Stack.Screen name="Dashboard" component={Dashboard} />
     <Stack.Screen name="AddBook" component={AddBook} />
      <Stack.Screen name="UpdateBook" component={AddBook} />
     </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;